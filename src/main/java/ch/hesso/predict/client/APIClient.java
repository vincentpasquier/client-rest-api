/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.client;

import ch.hesso.commons.AbstractEntity;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.Iterator;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class APIClient<T extends AbstractEntity> {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( APIClient.class );

	public static final String REST_API;

	static {
		String propertyAPIURL = System.getProperty ( "predict.api.url" );
		if ( propertyAPIURL == null ) {
			LOG.info ( "RESTful API property not set (predict.api.url). Using default path." );
			REST_API = "http://localhost:8080/core-rest/api";
		} else {
			REST_API = propertyAPIURL;
		}
	}

	//
	protected final String _apiRef;

	//
	protected final String _resource;

	//
	protected final Class<T> _$class;

	//
	protected final Client _client;

	//
	protected final WebTarget _target;

	//
	protected final String _fullResourcePath;

	//
	protected final String _session;

	/**
	 * @param apiRef
	 * @param resource
	 * @param $class
	 */
	protected APIClient (
			final String apiRef,
			final String resource,
			final Class<T> $class,
			final String session ) {
		_apiRef = apiRef;
		_resource = resource;
		_$class = $class;
		_client = JerseyClientBuilder.newBuilder ().build ();
		_target = _client.target ( _apiRef );
		_fullResourcePath = _apiRef + "/" + _resource + "/";
		_session = session;
	}

	/**
	 * @param apiRef
	 * @param resource
	 * @param $class
	 * @param <T>
	 *
	 * @return
	 */
	public static <T extends AbstractEntity> APIClient<T> create ( final String apiRef, final String resource, final Class<T> $class ) {
		return new APIClient<> ( apiRef, resource, $class, "" );
	}

	/**
	 * @param apiRef
	 * @param resource
	 * @param $class
	 * @param <T>
	 *
	 * @return
	 */
	public static <T extends AbstractEntity> APIClient<T> create ( final String apiRef, final String resource, final Class<T> $class, final String session ) {
		return new APIClient<> ( apiRef, resource, $class, session );
	}

	/**
	 * @param id
	 *
	 * @return
	 */
	public T get ( final String id ) {
		try {
			T object = _target
					.path ( _resource )
					.path ( id )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.get ( _$class );
			return object;
		} catch ( Exception e ) {
			return null;
		}
	}

	/**
	 * @return
	 */
	public Iterator<T> getAll () {
		Iterator<T> iterator = new SimpleIterator<> ( _apiRef, _resource, _$class );
		return iterator;
	}

	/**
	 * @param key
	 *
	 * @return
	 */
	public T getByKey ( final String key ) {
		try {
			return _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.path ( "key" )
					.path ( key )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.get ( _$class );
		} catch ( Exception e ) {
			return null;
		}
	}

	/**
	 * @param entity
	 */
	public String post ( final T entity ) {
		try {
			return _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.post ( Entity.json ( entity ) ).readEntity ( String.class );
		} catch ( Exception e ) {
			return null;
		}
	}

	/**
	 * @param entity
	 */
	public void put ( final T entity ) {
		_target
				.queryParam ( "sessionId", _session )
				.path ( _resource )
				.path ( entity.getId () )
				.request ( MediaType.APPLICATION_JSON_TYPE )
				.put ( Entity.json ( entity ), _$class );
	}

	/**
	 * @param entity
	 */
	public void delete ( final T entity ) {
		_target
				.queryParam ( "sessionId", _session )
				.path ( _resource )
				.path ( entity.getId () )
				.request ( MediaType.APPLICATION_JSON_TYPE )
				.delete ();
	}

	/**
	 * @return
	 */
	protected String apiRef () {
		return _apiRef;
	}
}
