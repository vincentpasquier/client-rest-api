/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.client.resources;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.ArrayIterator;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.restful.Conference;

import javax.ws.rs.core.MediaType;
import java.util.Iterator;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class CFPClient extends APIClient<Cfp> {

	/**
	 * @param apiRef
	 */
	protected CFPClient ( final String apiRef, final String session ) {
		super ( apiRef, Cfp.RESOURCE_PATH, Cfp.class, session );
	}

	/**
	 * @param apiRef
	 *
	 * @return
	 */
	public static CFPClient create ( final String apiRef ) {
		return new CFPClient ( apiRef, "" );
	}

	/**
	 * @param apiRef
	 *
	 * @return
	 */
	public static CFPClient create ( final String apiRef, final String session ) {
		return new CFPClient ( apiRef, session );
	}

	/**
	 * @return
	 */
	@Override
	public Iterator<Cfp> getAll () {
		return new ArrayIterator<> ( super.apiRef (), Cfp.RESOURCE_PATH, Cfp.class );
	}

	public Conference getConference ( final Cfp cfp ) {
		try {
			Conference response =
					_target
							.path ( _resource )
							.path ( cfp.getId () )
							.path ( "conference" )
							.request ( MediaType.APPLICATION_JSON_TYPE )
							.get ( Conference.class );
			return response;
		} catch ( Exception e ) {
			return null;
		}
	}

	public static boolean isConferenceValid ( final Conference conference ) {
		return conference != null;
	}
}
