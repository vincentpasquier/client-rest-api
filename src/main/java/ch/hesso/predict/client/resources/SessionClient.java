/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.client.resources;

import org.glassfish.jersey.client.JerseyClientBuilder;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class SessionClient {

	//
	private final WebTarget _target;

	//
	private final String RESOURCE = "sessions";

	//
	private String _session = "";

	private SessionClient ( final String apiRef ) {
		final String _apiRef = apiRef;
		final Client _client = JerseyClientBuilder.newBuilder ().build ();
		_target = _client.target ( _apiRef );
	}

	public static SessionClient create ( final String apiRef ) {
		return new SessionClient ( apiRef );
	}

	public String createSession () {
		try {
			_session = _target
					.path ( RESOURCE )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.get ( String.class );
		} catch ( Exception e ) {
		}
		return _session;
	}

	public String commitSession () {
		try {
			return _session = _target
					.path ( RESOURCE )
					.path ( _session )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.post ( Entity.json ( _session ) ).readEntity ( String.class );
		} catch ( Exception e ) {
			return null;
		}
	}

	public String deleteSession () {
		try {
			_session = _target
					.path ( RESOURCE )
					.path ( _session )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.delete ().readEntity ( String.class );
		} catch ( Exception e ) {
		}
		return _session;
	}

	public String session () {
		return _session;
	}

}
