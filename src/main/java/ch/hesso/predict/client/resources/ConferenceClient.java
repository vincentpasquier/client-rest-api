/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.client.resources;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.ArrayIterator;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.restful.Conference;
import ch.hesso.predict.restful.Ranking;
import ch.hesso.predict.restful.Venue;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ConferenceClient extends APIClient<Conference> {

	/**
	 * @param apiRef
	 */
	protected ConferenceClient ( final String apiRef, final String session ) {
		super ( apiRef, Conference.RESOURCE_PATH, Conference.class, session );
	}

	/**
	 * @param apiRef
	 *
	 * @return
	 */
	public static ConferenceClient create ( final String apiRef ) {
		return new ConferenceClient ( apiRef, "" );
	}

	/**
	 * @param apiRef
	 *
	 * @return
	 */
	public static ConferenceClient create ( final String apiRef, final String session ) {
		return new ConferenceClient ( apiRef, session );
	}

	/**
	 * @return
	 */
	@Override
	public Iterator<Conference> getAll () {
		return new ArrayIterator<> ( super.apiRef (), Conference.RESOURCE_PATH, Conference.class );
	}

	/**
	 * @param conference
	 *
	 * @return
	 */
	public Map<String, Venue> getVenues ( final Conference conference ) {
		Map<String, Venue> venues = new HashMap<> ();
		try {
			Object response =
					_target
							.path ( conference.getId () )
							.path ( "venues" )
							.request ( MediaType.APPLICATION_JSON_TYPE )
							.get ( Array.newInstance ( _$class, 0 ).getClass () );
			Venue[] objects = (Venue[]) response;
			for ( Venue venue : objects ) {
				venues.put ( venue.getKey (), venue );
			}
		} catch ( Exception e ) {
		}
		return venues;
	}

	public String addVenue ( final String conference, final String venue ) {
		try {
			Invocation invocation
					= _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.path ( conference )
					.path ( "venue" )
					.path ( venue )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.buildPost ( null );
			return invocation.invoke ( String.class );
		} catch ( Exception e ) {
			return null;
		}
	}

	public String addRanking ( final Ranking ranking ) {
		try {
			Invocation invocation
					= _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.path ( "ranking" )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.buildPost ( Entity.json ( ranking ) );
			return invocation.invoke ( String.class );
		} catch ( Exception e ) {
			return null;
		}
	}

	public String addCfp ( final Cfp cfp ) {
		try {
			Invocation invocation
					= _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.path ( "cfp" )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.buildPost ( Entity.json ( cfp ) );
			return invocation.invoke ( String.class );
		} catch ( Exception e ) {
			return null;
		}
	}
}
