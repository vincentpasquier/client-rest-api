/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.client.resources;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.ArrayIterator;
import ch.hesso.predict.restful.Person;
import ch.hesso.predict.restful.Publication;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PeopleClient extends APIClient<Person> {

	/**
	 * @param apiRef
	 */
	protected PeopleClient ( final String apiRef, final String session ) {
		super ( apiRef, Person.RESOURCE_PATH, Person.class, session );
	}

	/**
	 * @param apiRef
	 *
	 * @return
	 */
	public static PeopleClient create ( final String apiRef ) {
		return new PeopleClient ( apiRef, "" );
	}

	/**
	 * @param apiRef
	 *
	 * @return
	 */
	public static PeopleClient create ( final String apiRef, final String session ) {
		return new PeopleClient ( apiRef, session );
	}

	/**
	 * @return
	 */
	@Override
	public Iterator<Person> getAll () {
		return new ArrayIterator<> ( super.apiRef (), Person.RESOURCE_PATH, Person.class );
	}

	public String addPublication ( final String personId, final String publication ) {
		try {
			Invocation invocation
					= _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.path ( personId )
					.path ( "publication" )
					.path ( publication )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.buildPost ( null );
			return invocation.invoke ( String.class );
		} catch ( Exception e ) {
			return null;
		}
	}

	public String addConference ( final String pcId, final String conferenceKey ) {
		try {
			Invocation invocation
					= _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.path ( pcId )
					.path ( "conference" )
					.path ( conferenceKey )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.buildPost ( null );
			return invocation.invoke ( String.class );
		} catch ( Exception e ) {
			return null;
		}
	}

	/**
	 * @param author
	 *
	 * @return
	 */
	public Person getByName ( final String author ) {
		try {
			Invocation invocation
					= _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.path ( "/name" )
					.path ( author )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.buildGet ();
			return invocation.invoke ( Person.class );
		} catch ( Exception e ) {
			return null;
		}
	}

	public List<Publication> getPublications ( final String id ) {
		try {
			Invocation invocation
					= _target
					.queryParam ( "sessionId", _session )
					.path ( _resource )
					.path ( id )
					.path ( "publications" )
					.request ( MediaType.APPLICATION_JSON_TYPE )
					.buildGet ();
			return invocation.invoke ( new GenericType<List<Publication>> () {
			} );
		} catch ( Exception e ) {
			return new ArrayList<> ();
		}
	}
}
