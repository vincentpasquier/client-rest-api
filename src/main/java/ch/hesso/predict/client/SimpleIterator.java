/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.client;

import org.glassfish.jersey.client.JerseyClientBuilder;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class SimpleIterator<E> implements Iterator<E> {

	//
	private final String _resource;

	//
	private final Class<E> _$class;

	//
	private final WebTarget _target;

	//
	private final List<E> _elements;

	//
	private Iterator<E> _iterator;

	/**
	 * @param apiRef
	 * @param resource
	 * @param $class
	 */
	@SuppressWarnings ("unchecked") // Checked typecast
	public SimpleIterator ( final String apiRef, final String resource, final Class<E> $class ) {
		_resource = resource;
		_$class = $class;
		Client _client = JerseyClientBuilder.newBuilder ().build ();
		_target = _client.target ( apiRef );
		_elements = new ArrayList<> ();
		WebTarget query = _target.path ( _resource );    // Get all returns returns an array of objects type E
		// Since the creation of ArrayIterator involves passing the entity class
		// We can safely typecast it
		Object response = query.request ( MediaType.APPLICATION_JSON_TYPE ).get ( Array.newInstance ( _$class, 0 ).getClass () );
		// Returns an array of E type since we pass it an array of corresponding type.
		E[] objects = (E[]) response;
		_elements.addAll ( Arrays.<E>asList ( objects ) );
		_iterator = _elements.iterator ();
	}

	@Override
	public boolean hasNext () {
		return !_elements.isEmpty ();
	}

	@Override
	public E next () {
		E next = _iterator.next ();
		return next;
	}

	@Override
	public void remove () {
		throw new AssertionError ( "No remove for this iterator!" );
	}
}
