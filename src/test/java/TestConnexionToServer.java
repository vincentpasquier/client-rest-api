/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.CFPClient;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.restful.Plugin;

import java.util.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestConnexionToServer {

	public static void main ( final String[] args ) {
		CFPClient cfpClient = CFPClient.create ( APIClient.REST_API );
		Iterator<Cfp> cfps = cfpClient.getAll ();
		while ( cfps.hasNext () ) {
			Cfp cfp = cfps.next ();
			System.out.println ( cfp.toString () );
		}

		APIClient<Plugin> pluginClient = APIClient.create ( APIClient.REST_API, "plugins", Plugin.class );

		Plugin post = new Plugin ();

		post.setName ( "bl1uman" );
		post.setWebSocket ( "ws://localhost" );

		Map<String, Set<HTTPMethod>> registrations = new HashMap<> ();

		registrations.put ( Plugin.ROOT_NAME, new HashSet<> ( Arrays.<HTTPMethod>asList ( HTTPMethod.DELETE ) ) );
		post.setRegistrations ( registrations );
		pluginClient.post ( post );

		Iterator<Plugin> plugins = pluginClient.getAll ();

		while ( plugins.hasNext () ) {
			Plugin plugin = plugins.next ();
			System.out.println ( plugin );
		}
	}
}
