/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.restful.WebPage;
import com.google.common.io.Files;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestWebPageBulkOperations {

	@Test
	public void bulk () throws IOException, InterruptedException {
		String file = TestWebPageBulkOperations.class.getResource ( "/websites.txt" ).getFile ();
		Set<String> urls = new HashSet<> ( Files.readLines ( new File ( file ), Charset.defaultCharset () ) );
		List<String> urlList = new ArrayList<> ( urls );
		System.out.println ( urlList.size () );
		List<BulkQuery> bulks = new ArrayList<> ( 10 );
		for ( int i = 0; i < 10; i++ ) {
			bulks.add ( new BulkQuery ( urlList, 10 ) );
		}
		ExecutorService executorService = Executors.newFixedThreadPool ( 10 );
		executorService.invokeAll ( bulks );
		executorService.awaitTermination ( 1000, TimeUnit.HOURS );
	}

	public static final class BulkQuery implements Callable<Void> {

		private static int ID_BULK = 0;

		private final int id = ID_BULK++;

		private int pos = id;

		private final List<String> _urls;

		private final int _number;

		public BulkQuery ( final List<String> urls, final int number ) {
			_urls = urls;
			_number = number;
		}

		@Override
		public Void call () throws Exception {
			WebPageClient webPageClient = WebPageClient.create ( APIClient.REST_API );
			WebPage page = new WebPage ();
			while ( pos < _urls.size () ) {
				String url = _urls.get ( pos );
				page.setUrl ( url );
				webPageClient.getWebPage ( page );
				System.out.println ( pos );
				//System.out.println ( url );
				pos += id + _number;
			}
			return null;
		}
	}

}
