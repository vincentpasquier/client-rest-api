/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */

import ch.hesso.predict.client.resources.VenueClient;
import ch.hesso.predict.restful.Venue;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestVenueOperations extends TestsParameters {

	private static VenueClient venueClient = VenueClient.create ( API_PATH );

	private static String something;

	private static Venue venue;

	private static String venueId;

	@BeforeClass
	public static void addVenue () {
		venue = new Venue ();
		something = "somewhere" + R.nextDouble () + "aufeur the rainbows";
		venue.setKey ( something );
		venue.setYear ( 2077 );
		venueId = venueClient.post ( venue );
	}

	@Test
	public void getAllVenues () {
		Assert.assertTrue ( venueClient.getAll ().hasNext () );
	}

	@Test
	public void getById () {
		Assert.assertTrue ( venueClient.get ( venueId ).getKey ().equals ( something ) );
	}

}
